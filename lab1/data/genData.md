# _genData.csv_ information

The CSV contains the probabilities of buying a products based on different
user stereotypes.

The different columns correspond to different product types. There are 5 types:
  - FASHION			
  - TECH (+sport,+entertainment)
  - HOME (+tech,+sport)
  - SPORT
  - ENTERTAINMENT (high amount of purchases)
 + PROBABILITY OF EACH TYPE OF USER TO BE THE NEXT USER REGISTERED

  The different rows correspond to different user stereotypes. There are 5 types:
  - STANDARD WOMAN (+fashion, +home)
  - STANDARD MAN (+sport,+entertainment)
  - YOUNG (+tech,+sport)
  - ENTREPRENEUR (high amount of purchases)
  - ELDER (low amount of purchases)
 + PROBABILITY OF EACH TYPE OF PRODUCT TO BE THE NEXT PRODUCT INCLUDED IN THE CATALOG
