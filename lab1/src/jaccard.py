import numpy

def get_tuples(products,customers,purchases):
	#similarities = get_similarities(products,customers,purchases)

	items = purchases.shape[1]
	similarities = numpy.zeros((items,items))
	recommendations = []
	for p in range(products):
		for p2 in range(p + 1, products):
			#recommendations.append((p, p2, len(similarities[p] & similarities[p2]) / len(similarities[p] | similarities[p2])))
			recommendations.append((p,p2,similarity(purchases[:,p],purchases[:,p2])))


	return recommendations

def get_table(products,customers,purchases,method='Fran'):
	if method == 'Fran':
		items = purchases.shape[1]

		similarities = numpy.zeros((items,items))
		for i1 in range(items):
			for i2 in range(items):
				similarities[i1][i2] = similarity(purchases[:,i1],purchases[:,i2])

		recommendations = similarities

	elif method == 'Alvaro':
		similarities = get_similarities(products,customers,purchases)

		recommendations = numpy.empty([products, products])
		for p in range(products):
			for p2 in range(products):
				recommendations[p,p2] = len(similarities[p] & similarities[p2]) / len(similarities[p] | similarities[p2])

	else:
		raise Exception('Method not defined')

	return recommendations

def get_similarities(products,customers,purchases):
	similarities = []
	for p in range(products):
		p_set = set()
		for c in range(customers):
			if (purchases[p, c] == 1):
				p_set.add(c)
		similarities.append(p_set)

	return similarities

def distance(user,recommendations,purchases):
	user_purchases = numpy.where(purchases[user,:] == 1)
	#user_purchases2, _ = numpy.where(purchases[user,:] == 1)

	user_purchases = user_purchases[0]
	print("These are the user purchases : {}".format(user_purchases))
	print(user_purchases)

	user_items = []
	for p in range(len(user_purchases)):
		user_items.extend(list(filter(lambda y:(y[0] == user_purchases[p] and y[1] not in user_purchases)
		                              or (y[1] == user_purchases[p] and y[0] not in user_purchases), 
		                              recommendations)))
	
	print("These are the user items : {}".format(user_items))
	return [(x[2], get_opposite(x, user_purchases)) for x in user_items]

def get_opposite(tuple3,user_purchases):
	return tuple3[0] if tuple3[0] not in user_purchases else tuple3[1]

def similarity(i1,i2):
			cUnion = numpy.count_nonzero(numpy.logical_or(i1,i2))
			cIntersection = numpy.count_nonzero(numpy.logical_and(i1,i2))
			if cUnion == 0:
				return 0
			else:
				return cIntersection/float(cUnion)