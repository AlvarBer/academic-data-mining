import gen_data
from tabulate import tabulate
import jaccard
import numpy
import pretty

customers = 9
products = 9


purchases = gen_data.with_csv(products,customers)
products = purchases.shape[1]
pretty.print_purchases(purchases)

#purchases = gen_data.simply_random(customers, products)
#pretty.print_purchases(purchases)

#purchases = gen_data.hard_coded()

user = str(input("What user do you want to get the item recommendation?: "))
while not user.isdigit():
	user = input("Please introduce a number: ")
user = int(user)

fran_recommendations = jaccard.get_table(products, customers, purchases)
print(tabulate(fran_recommendations,tablefmt='fancy_grid'))

#alvaro_recommendations = jaccard.get_table(products, customers, purchases, method='Alvaro')
#print(tabulate(alvaro_recommendations))

recommendations = jaccard.get_tuples(products, customers, purchases)
print(recommendations)
pretty.print_recommended(recommendations, user,purchases)
