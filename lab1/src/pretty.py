import tabulate
import numpy
import jaccard

def print_purchases(purchases):
	product_header = []
	for product in range(purchases.shape[1]):
		product_header.append("Item {product_number}".format(product_number=product))
	
	user_header = numpy.empty([purchases.shape[0], 1], dtype="object_")
	for user in range(purchases.shape[0]):
		user_header[user,0] = "User {user_number}".format(user_number=user)
	
	full_table = numpy.concatenate((user_header,purchases),axis=1)
	
	print(tabulate.tabulate(full_table,headers=product_header,tablefmt="plain", numalign="center"))

def print_recommended(recommendations,user,purchases):
	distances = jaccard.distance(user, recommendations, purchases)
	print("The distances are: {}".format(distances))
	best_item = max(distances)
	print("Closest item according jaccard {}".format(best_item[1]))
