import numpy as np
import random

def with_csv(items,customers):
    'Generates a table of n customers-items based on customer-stereotype probabilities'

    #Load the probabilities table
    prob = np.around(np.genfromtxt('../data/genData.csv', delimiter=';', skip_header=1), decimals=4)
    [rows,columns] = prob.shape

    #Number of different item types
    item_types_n = columns - 1;

    #Saves in a vector the probabilities of a item to appear in the catalog
    item_prob = []
    for i in range(columns-1):
        item_prob.append(prob[rows-1][i])

    #print ("The probabilities for each item type are: {}\n".format(item_prob))

    #Generates a vector with the type of item in each column
    item_types_vector = []
    for i in range(items):
        #Select the type of item to insert in the catalog
        item_sel = np.random.choice(item_types_n, 1, item_prob)
        item_types_vector.append(item_sel[0])

    #print ("The type for each item type is: {}\n".format(item_types_vector))

    #Number of different customer types
    customer_types_n = rows - 1;

    #Saves in a vector the probabilities of a customer type to register in the system
    customer_prob = []
    for i in range(rows-1):
        customer_prob.append(prob[i][columns-1])

    #print ("The probabilities for each user type are: {}\n".format(customer_prob))

    #Generates a vector with the type of user in each row
    cust_types_vector = []
    for i in range(customers):
        #Select the type of item to insert in the catalog
        cust_sel = np.random.choice(customer_types_n, 1, customer_prob)
        cust_types_vector.append(cust_sel[0])

    #print ("The type for each customer is: {}\n".format(cust_types_vector))

    purchases = np.empty([customers,items])

    for i in range(customers):
        for j in range(items):
            probability = prob[cust_types_vector[i]][item_types_vector[j]]
            #print ("The probability of user of type {} to buy an item of type {} is: {}\n".format \
            #(cust_types_vector[i],item_types_vector[j],probability))
            purchases[i][j] = np.random.binomial(1, probability)
    return purchases

def simply_random(items, customers):
    items_bought = np.zeros((items, customers))
    for p in range(items):
        for c in range(customers):
            if random.gauss(.5, .1) > .5:
                items_bought[p, c] = 1
    return items_bought

def hard_coded():
    table = np.array([[1,0],[0,1],[0,1],[1,0],[1,1],[0,1],[1,0],[1,1],[0,1],[1,0]])
    print(table)
    return table
